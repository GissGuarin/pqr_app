require 'test_helper'

class PqrTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pqr_type = pqr_types(:one)
  end

  test "should get index" do
    get pqr_types_url
    assert_response :success
  end

  test "should get new" do
    get new_pqr_type_url
    assert_response :success
  end

  test "should create pqr_type" do
    assert_difference('PqrType.count') do
      post pqr_types_url, params: { pqr_type: { day: @pqr_type.day, name: @pqr_type.name } }
    end

    assert_redirected_to pqr_type_url(PqrType.last)
  end

  test "should show pqr_type" do
    get pqr_type_url(@pqr_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_pqr_type_url(@pqr_type)
    assert_response :success
  end

  test "should update pqr_type" do
    patch pqr_type_url(@pqr_type), params: { pqr_type: { day: @pqr_type.day, name: @pqr_type.name } }
    assert_redirected_to pqr_type_url(@pqr_type)
  end

  test "should destroy pqr_type" do
    assert_difference('PqrType.count', -1) do
      delete pqr_type_url(@pqr_type)
    end

    assert_redirected_to pqr_types_url
  end
end
