class AddpqrTypeToPqrs < ActiveRecord::Migration[5.1]
  def change
  add_reference :pqrs, :pqr_type, foreign_key: true
  end
end
