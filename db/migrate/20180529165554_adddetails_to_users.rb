class AdddetailsToUsers < ActiveRecord::Migration[5.1]
  def change
  add_column :users, :name, :string
  add_column :users, :last_name, :string
  add_column :users, :address, :string 
  add_column :users, :identification, :integer
  add_column :users, :date_of_birth, :datetime 
  add_column :users, :female, :boolean, default:false
  end
end
