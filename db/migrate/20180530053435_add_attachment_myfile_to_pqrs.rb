class AddAttachmentMyfileToPqrs < ActiveRecord::Migration[5.1]
  def self.up
    change_table :pqrs do |t|
      t.attachment :myfile
    end
  end

  def self.down
    remove_attachment :pqrs, :myfile
  end
end
