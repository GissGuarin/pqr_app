class AdduserIdToPqrs < ActiveRecord::Migration[5.1]
  def change
    add_reference :pqrs, :user, foreign_key: true
  end
end
