class CreatePqrTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :pqr_types do |t|
      t.string :name
      t.integer :day

      t.timestamps
    end
  end
end
