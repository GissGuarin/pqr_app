Rails.application.routes.draw do

  
  
  get 'home/welcome'

  root 'home#welcome'
 
   resources :user_types
   resources :pqr_types
 
  resources :pqrs do
  resources :comments, only:[:create, :destroy, :update]
  end
   devise_for :users, :controllers => {:registrations => "users/registrations"}
 
  

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
end
