class PqrTypesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_pqr_type, only: [:show, :edit, :update, :destroy]

  # GET /pqr_types
  # GET /pqr_types.json
  def index
    @pqr_type = PqrType.all
  end

  # GET /pqr_types/1
  # GET /pqr_types/1.json
  def show
  end

  # GET /pqr_types/new
  def new
     @pqr_type = PqrType.new     
  end

  # GET /pqr_types/1/edit
  def edit
  end

  # POST /pqr_types
  # POST /pqr_types.json
  def create
    @pqr_type = PqrType.new(pqr_type_params)

    respond_to do |format|
      if @pqr_type.save
        format.html { redirect_to @pqr_type, notice: 'Pqr type was successfully created.' }
        format.json { render :show, status: :created, location: @pqr_type }
      else
        format.html { render :new }
        format.json { render json: @pqr_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pqr_types/1
  # PATCH/PUT /pqr_types/1.json
  def update
    respond_to do |format|
      if @pqr_type.update(pqr_type_params)
        format.html { redirect_to @pqr_type, notice: 'Pqr type was successfully updated.' }
        format.json { render :show, status: :ok, location: @pqr_type }
      else
        format.html { render :edit }
        format.json { render json: @pqr_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pqr_types/1
  # DELETE /pqr_types/1.json
  def destroy
    @pqr_type.destroy
    respond_to do |format|
      format.html { redirect_to pqr_types_url, notice: 'Pqr type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pqr_type
      @pqr_type = PqrType.find(params[:id])
    end
    

    # Never trust parameters from the scary internet, only allow the white list through.
    def pqr_type_params
      params.require(:pqr_type).permit(:name, :day)
    end

end
