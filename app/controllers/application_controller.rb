class ApplicationController < ActionController::Base

  before_action :configure_permitted_parameters, if: :devise_controller?
  
  protected
     def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :last_name, :address, :identification, :email, :password, :current_password, :female, :date_of_birth, :user_type_id])
       devise_parameter_sanitizer.permit(:account_update, keys: [:name, :last_name, :address, :identification, :email, :password, :current_password, :female, :date_of_birth, :user_type_id])
   end
   
end
