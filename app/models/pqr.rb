class Pqr < ApplicationRecord
    belongs_to :user
    belongs_to :pqr_type
    has_many :comments
    has_attached_file :myfile
    validates_attachment_content_type :myfile, content_type: ["application/pdf","application/xlsx","application/docx","application/doc","image/jpg","image/jpeg"] 
end
