json.extract! pqr_type, :id, :name, :day, :created_at, :updated_at
json.url pqr_type_url(pqr_type, format: :json)
