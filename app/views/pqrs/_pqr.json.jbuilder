json.extract! pqr, :id, :subject, :content, :created_at, :updated_at
json.url pqr_url(pqr, format: :json)
